const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Header = require("../models/Header");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// dashboar route

router.get("/", async (req, res) => {
    res.render("admin/dashboard", { layout: false });
});

//  add header route
router.get("/addheader", async (req, res) => {
    res.render("admin/addheader", { layout: false });
});

// Add a gig
router.post("/addheader", (req, res) => {
    let { title, slogan } = req.body;
    let errors = [];

    // Validate Fields
    if (!title) {
        errors.push({ text: "Please add a title" });
    }

    if (!slogan) {
        errors.push({ text: "Please add some slogan" });
    }

    // Check for errors
    if (errors.length > 0) {
        res.render("admin/addheader", {
            errors,
            title,
            slogan,
        });
    } else {
        if (!slogan) {
            slogan = "Unknown";
        } else {
            slogan = `$${slogan}`;
        }

        // Insert into table
        Header.create({
            title,
            slogan,
        })
            .then((header) => {
                Header.findAll({
                    limit: 1,
                    where: {},
                    order: [["createdAt", "DESC"]],
                })
                    .then((data) => {
                        console.log(data[0]);
                        res.render("home/home",{data})
                    })
                    .catch((err) => res.render("error", { error: err }));
            })

            .catch((err) => res.render("error", { error: err.message }));
    }
});
// // //edit for
// router.get("/memberedit/:id", (req, res) => {
//     // console.log( Gig.findByPk(1));
//     Member.findByPk(req.params.id)
//         .then((member) => res.render("codegig/memberedit", { member }))
//         .catch((err) => res.render("error", { error: err }));
// });

// // Search for members
// router.get("/search", (req, res) => {
//     let { term } = req.query;

//     // Make lowercase
//     term = term.toLowerCase();

//     Member.findAll({ where: { name: { [Op.like]: "%" + term + "%" } } })
//         .then((members) => res.render("codegig/members", { members }))
//         .catch((err) => res.render("error", { error: err }));
// });
// // // Update
// router.post("/updatemember/:id", (req, res) => {
//     let {
//      name,
//      image,
//      sex
//     } = req.body;
//     Member.update(
//         {
//             name,
//             image,
//             sex
//         },
//         { where: { id: req.params.id } }
//     )
//         .then((members) => {
//             // res.send("successfully updated");
//               res.redirect("/codegig/members")
//         })
//         .catch((error) => {
//             console.log(error);
//             res.status(404).send(error);
//         });
// });

// // // delete
// router.post("/deletemember/:id", (req, res) => {

//     Member.destroy({
//                 where: { id: req.params.id },
//             })
//                 .then(() =>  res.redirect("/codegig/members"))

//                 .catch((error) => {
//                     console.log(error);
//                     res.status(404).send(error);
//                 });
// });

// Delete
// router.delete("/deletegigs/:", (req, res) => {
//     gigs.destroy({
//         where: { id: 1 },
//     })
//         .then(() => {
//             res.send("gigs successfully deleted");
//         })
//         .catch((error) => {
//             console.log(error);
//             res.status(404).send(error);
//         });
// });

module.exports = router;
