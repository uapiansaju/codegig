const express = require('express'); 
const { get } = require('../admin/admin');

const router = express.Router();


router.get('', async(req, res) => {
  res.render('home/home', {layout: false});
})

module.exports = router;