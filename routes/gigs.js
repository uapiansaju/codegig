const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Gig = require("../models/Gig");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// Get gig list
router.get("/", (req, res) => {
    Gig.findAll()
        .then((gigs) =>
            res.render("codegig/gigs", {
                gigs,
            })
        )
        .catch((err) => res.render("error", { error: err }));

    // Display add gig form
    router.get("/add", (req, res) => res.render("codegig/add"));
});

// Add a gig
router.post("/add", (req, res) => {
    let {
        title,
        technologies,
        birthday,
        age,
        mobile,
        budget,
        description,
        contact_email,
    } = req.body;
    let errors = [];

    // Validate Fields
    if (!title) {
        errors.push({ text: "Please add a title" });
    }
    if (!technologies) {
        errors.push({ text: "Please add some technologies" });
    }
    if (!birthday) {
        errors.push({ text: "Please add some technologies" });
    }
    if (!age) {
        errors.push({ text: "Please add some technologies" });
    }
    if (!mobile) {
        errors.push({ text: "Please add some technologies" });
    }
    if (!description) {
        errors.push({ text: "Please add a description" });
    }
    if (!contact_email) {
        errors.push({ text: "Please add a contact email" });
    }

    // Check for errors
    if (errors.length > 0) {
        res.render("add", {
            errors,
            title,
            technologies,
            birthday,
            age,
            mobile,
            budget,
            description,
            contact_email,
        });
    } else {
        if (!budget) {
            budget = "Unknown";
        } else {
            budget = `$${budget}`;
        }

        // Make lowercase and remove space after comma
        technologies = technologies.toLowerCase().replace(/,[ ]+/g, ",");

        // Insert into table
        Gig.create({
            title,
            technologies,
            birthday,
            age,
            mobile,
            description,
            budget,
            contact_email,
        })
            .then((gig) => res.redirect("/gigs"))
            .catch((err) => res.render("error", { error: err.message }));
    }
});
//edit
router.get("/gigsedit/:id", (req, res) => {
    // console.log( Gig.findByPk(1));
    Gig.findByPk(req.params.id)
        .then((gig) => res.render("codegig/gigsedit", { gig }))
        .catch((err) => res.render("error", { error: err }));
});

// Search for gigs
router.get("/search", (req, res) => {
    let { term } = req.query;

    // Make lowercase
    term = term.toLowerCase();

    Gig.findAll({ where: { title: { [Op.like]: "%" + term + "%" } } })
        .then((gigs) => res.render("codegig/gigs", { gigs }))
        .catch((err) => res.render("error", { error: err }));
});
// Update
router.post("/updategigs/:id", (req, res) => {
    let {
        title,
        technologies,
        birthday,
        age,
        mobile,
        budget,
        description,
        contact_email,
    } = req.body;
    Gig.update(
        {
            title,
            technologies,
            birthday,
            age,
            mobile,
            budget,
            description,
            contact_email,
        },
        { where: { id: req.params.id } }
    )
        .then((gigs) => {
            // res.send("successfully updated");
              res.redirect("/gigs")
        })
        .catch((error) => {
            console.log(error);
            res.status(404).send(error);
        });
});

// delete
router.post("/deletegig/:id", (req, res) => {
    
    Gig.destroy({
                where: { id: req.params.id },
            })
                .then(() =>  res.redirect("/gigs"))
                   

                .catch((error) => {
                    console.log(error);
                    res.status(404).send(error);
                });
});





// Delete
// router.delete("/deletegigs/:", (req, res) => {
//     gigs.destroy({
//         where: { id: 1 },
//     })
//         .then(() => {
//             res.send("gigs successfully deleted");
//         })
//         .catch((error) => {
//             console.log(error);
//             res.status(404).send(error);
//         });
// });

module.exports = router;
