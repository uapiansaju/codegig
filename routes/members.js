const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Member = require("../models/Member");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// Get gig list
router.get("/", (req, res) => {
    Member.findAll()
        .then((members) =>
     
            res.render("codegig/members", {
                members,
            })
        )
        .catch((err) => res.render("error", { error: err }));

    // Display add gig form
    router.get("/addmember", (req, res) => res.render("codegig/addmember"));
});

// Add a gig
router.post("/addmember", (req, res) => {
    let {
        name,
        image,
        sex
    } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add a name" });
    }

    if (!image) {
        errors.push({ text: "Please add image" });
    }

    if (!sex) {
        errors.push({ text: "Please add some sex" });
    }
    
    // Check for errors
    if (errors.length > 0) {
        res.render("addmember", {
            errors,
            name,
            image,
             sex
        });
    } else {
        if (!name) {
            name = "Unknown";
        } else {
            name = `$${name}`;
        }

        // Make lowercase and remove space after comma
        sex = sex.toLowerCase().replace(/,[ ]+/g, ",");

        // Insert into table
        Member.create({
            name,
            image,
            sex
          
        })
            .then((member) => res.redirect("/members"))
            .catch((err) => res.render("error", { error: err.message }));
    }
});
// //edit for 
router.get("/memberedit/:id", (req, res) => {
    // console.log( Gig.findByPk(1));
    Member.findByPk(req.params.id)
        .then((member) => res.render("codegig/memberedit", { member }))
        .catch((err) => res.render("error", { error: err }));
});

// Search for members
router.get("/search", (req, res) => {
    let { term } = req.query;

    // Make lowercase
    term = term.toLowerCase();

    Member.findAll({ where: { name: { [Op.like]: "%" + term + "%" } } })
        .then((members) => res.render("codegig/members", { members }))
        .catch((err) => res.render("error", { error: err }));
});
// // Update
router.post("/updatemember/:id", (req, res) => {
    let {
     name,
     image,
     sex
    } = req.body;
    Member.update(
        {
            name,
            image,
            sex
        },
        { where: { id: req.params.id } }
    )
        .then((members) => {
            // res.send("successfully updated");
              res.redirect("/members")
        })
        .catch((error) => {
            console.log(error);
            res.status(404).send(error);
        });
});

// // delete
router.post("/deletemember/:id", (req, res) => {
    
    Member.destroy({
                where: { id: req.params.id },
            })
                .then(() =>  res.redirect("/members"))
                   

                .catch((error) => {
                    console.log(error);
                    res.status(404).send(error);
                });
});





// Delete
// router.delete("/deletegigs/:", (req, res) => {
//     gigs.destroy({
//         where: { id: 1 },
//     })
//         .then(() => {
//             res.send("gigs successfully deleted");
//         })
//         .catch((error) => {
//             console.log(error);
//             res.status(404).send(error);
//         });
// });

module.exports = router;
