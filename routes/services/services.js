const express = require("express");
const { get } = require("../admin/admin");

const router = express.Router();

//Products//
router.get("/blockchain", async (req, res) => {
    res.render("homesetup/services/blockchain", { layout: false });
  });
  router.get("/computervision", async (req, res) => {
    res.render("homesetup/services/computervision", { layout: false });
  });
  
  router.get("/dataprivacy-security", async (req, res) => {
    res.render("homesetup/services/dataprivacy-security", { layout: false });
  });
  router.get("/machine-learn", async (req, res) => {
    res.render("homesetup/services/machine-learn", { layout: false });
  });
  router.get("/blog", async (req, res) => {
    res.render("homesetup/blog/blog", { layout: false });
  });
  

module.exports = router;
