const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');
var session = require('express-session');
// Routes Import
const home = require('./routes/home/home');
const login = require('./routes/login/login');
const admin = require('./routes/admin/admin');
const team = require('./routes/team/team');
const dashboard = require('./routes/dashboard');

// Database
const db = require('./config/database');

// Test DB
db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error: ' + err))

const app = express();

// Handlebars
app.engine('hbs', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'hbs');

// Body Parser
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));
// IITech  route setup

// Routes
app.use('/', home);
app.use('/login', login);
app.use('/admin', admin);
app.use('/dashboard', dashboard);
app.use('/team',team);


// Index route
app.get('/codegig', (req, res) => res.render('codegig/index', { layout: 'landing' }));

// Gig routes
app.use('/gigs', require('./routes/gigs'));

//member routes
app.use('/members', require('./routes/members'));
//member routes
app.use('/dashboard', require('./routes/dashboard'));

const PORT = process.env.PORT || 3000;

app.listen(PORT, console.log(`Server started on port ${PORT}`));