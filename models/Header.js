const Sequelize = require('sequelize');
const db = require('../config/database');

const Header = db.define('header', {
  title: {
    type: Sequelize.STRING
  },

 slogan:   {
     type: Sequelize.STRING
 }


});

Header.sync().then(() => {
  console.log('header table created');
});
module.exports = Header;