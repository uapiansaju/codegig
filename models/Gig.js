const Sequelize = require('sequelize');
const db = require('../config/database');

const Gig = db.define('gig', {
  title: {
    type: Sequelize.STRING
  },
  technologies: {
    type: Sequelize.STRING
  },
  birthday: {
    type: Sequelize.STRING
  },
  age: {
    type: Sequelize.STRING
  },
  mobile: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  budget: {
    type: Sequelize.STRING
  },
  contact_email: {
    type: Sequelize.STRING
  }
});

Gig.sync().then(() => {
  console.log('table created');
});
module.exports = Gig;